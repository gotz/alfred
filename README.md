This repository contains Alfred workflows that I've created for my own use.  I'm sharing them here in case they are useful for others.

1. TerminalInFolder.alfredworkflow

This workflow opens a new terminal window and sets the working directory based on Alfred folder search.  It is activated with the 'term' keyword, which must be followed by a query string to locate a folder on your Mac.  For example:

term Desktop  --> This will open a terminal window with your Desktop folder as the root.

